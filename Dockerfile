FROM docker.io/alpine:3.17.1
RUN apk add --no-cache \
        bash \
        httpie \
        curl \
        drill \
        tcpdump \
        tshark \
        lsof \
        strace \
        nmap \
        jq
