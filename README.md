# tools

just alpine with some networking tools installed

* attach to a running container's network:
```
docker run --rm -ti --network container:${CONTAINER_ID} codeberg.org/mrc/tools:latest
```
* attach to a running container's PID space, with --cap-add SYS_PTRACE
```
docker run --rm -ti --cap-add SYS_PTRACE --pid container:${CONTAINER_ID} codeberg.org/mrc/tools:latest
```

